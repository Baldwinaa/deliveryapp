package com.example.delivery.deliveryapp.utils;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.delivery.deliveryapp.R;
import com.example.delivery.deliveryapp.remote.Http;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paulina on 10/04/2017.
 */

public class RegistrationFragment extends Fragment {

    private EditText mNameView;
    private EditText mLastNameView;
    private EditText mEmailView;
    private EditText mPasswordView;
    View view;
    private String response;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_registration, container, false);
        mNameView = (EditText) view.findViewById(R.id.name);
        mLastNameView = (EditText) view.findViewById(R.id.lastName);
        mEmailView = (EditText) view.findViewById(R.id.email);
        mPasswordView = (EditText) view.findViewById(R.id.password);
        Button mLoginButton = (Button) view.findViewById(R.id.sign_up_button);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSignUp();
            }
        });
        return view;
    }


    private void attemptSignUp() {
        mNameView.setError(null);
        mLastNameView.setError(null);
        mEmailView.setError(null);
        mPasswordView.setError(null);

        String name = mNameView.getText().toString();
        String lastName = mLastNameView.getText().toString();
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(name)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        }
        if (TextUtils.isEmpty(lastName)) {
            mLastNameView.setError(getString(R.string.error_field_required));
            focusView = mLastNameView;
            cancel = true;
        }
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            if(isRegistrationValid(name, lastName, email, password)){
                Toast.makeText(getActivity(), getString(R.string.correct_registration) , Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
            else {
                Toast.makeText(getActivity(), getString(R.string.incorrect_registration_data), Toast.LENGTH_LONG).show();
            }

        }
    }

    private boolean isRegistrationValid(String name, String lastName, String email, String password) {
        final Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put("name", name);
        parameterMap.put("lastName", lastName);
        parameterMap.put(getString(R.string.email), email);
        parameterMap.put(getString(R.string.password), password);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response = Http.Post(getString(R.string.registration_driver_url), parameterMap);
                }
                catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (response.contains("successfully")) {
            return true;
        }
        return false;
    }
}
