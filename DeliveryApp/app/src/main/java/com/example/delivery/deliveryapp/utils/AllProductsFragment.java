package com.example.delivery.deliveryapp.utils;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.RecoverySystem;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;

import com.example.delivery.deliveryapp.R;
import com.example.delivery.deliveryapp.model.DriverModel;
import com.example.delivery.deliveryapp.model.ProductModel;
import com.example.delivery.deliveryapp.remote.Http;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by AdelaK on 2017-05-13.
 */

public class AllProductsFragment extends Fragment {


    private View view;
    private ListView list;
    String response;
    private List productsList;
    private ArrayAdapter<ProductModel> adapter;
    private List<String> driversEmail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product_list, container, false);
        String result = getList(getString(R.string.get_packages_url));
        try {
            productsList = getListOfProductsModel(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ListView listView1 = (ListView) view.findViewById(R.id.listView);
        Button mGenerateButton = (Button) view.findViewById(R.id.generate_button);
        adapter = new ArrayAdapter<ProductModel>(view.getContext(), android.R.layout.simple_list_item_1, productsList);
        listView1.setAdapter(adapter);
        mGenerateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptGenerateLists(view.getContext());
            }
        });
        return view;
    }

    private void attemptGenerateLists(Context context) {
        final List<DriverModel> drivers = getItems();
        driversEmail = new ArrayList<String>();
        String[] alertDialogItems = new String[drivers.size()];
        for (int i=0; i < drivers.size(); i++){
            alertDialogItems[i]= drivers.get(i).toString();
        }
        new AlertDialog.Builder(getActivity())
                .setTitle("Select drivers")
                .setMultiChoiceItems(alertDialogItems, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which,
                                                boolean isChecked) {
                                if(isChecked) {
                                    driversEmail.add(drivers.get(which).getEmail());
                                }
                                else{
                                    driversEmail.remove(drivers.get(which).getEmail());
                                }
                            }
                        })
                .setNegativeButton("Cancel", null)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(driversEmail.size() > 0) {
                            sendDriversEmail(driversEmail);
                        }
                    }
                })
                .show();
    }

    private void sendDriversEmail(List<String> driversEmail) {
        final Map<String, String> parameterMap = new HashMap<>();
        for(int i=0; i<driversEmail.size();i++) {
            parameterMap.put(String.format("%s[%d]", getString(R.string.dEmails), i), driversEmail.get(i));
        }
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response = Http.Post(getString(R.string.assign_packages_url), parameterMap);
                }
                catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        getFragmentManager().beginTransaction().replace(R.id.content_main, new AllProductsFragment()).commit();
    }

    private List<DriverModel> getItems() {
        String result = getList(getString(R.string.get_drivers_url));
        try {
            List<DriverModel> drivers = getListOfDrivers(result);
            return drivers;
        } catch (JSONException e) {
            e.printStackTrace();
        }
      return null;
    }

    private String getList(final String url) {
        response = "";
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response = Http.Get(url,null);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    private List<ProductModel> getListOfProductsModel(String result) throws JSONException {
        JSONArray jsonArray = new JSONArray(result);
        List productList = new ArrayList<ProductModel>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            ProductModel product = new ProductModel();
            try {
                product.setId(jsonObject.get("identifier").toString());
                product.setReceiverAddress(jsonObject.get("receiverAddress").toString());
                product.setReceiverPostalCode(jsonObject.get("receiverPostalCode").toString());
                product.setWeight(jsonObject.get("weight").toString());
                productList.add(product);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return productList;
    }

    private List<DriverModel> getListOfDrivers(String result) throws JSONException {
            JSONArray jsonArray = new JSONArray(result);
            List driverList = new ArrayList<DriverModel>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                DriverModel driver = new DriverModel();
                try {
                    driver.setEmail(jsonObject.get("email").toString());
                    driver.setName(jsonObject.get("name").toString());
                    driver.setLastName(jsonObject.get("lastName").toString());
                    driverList.add(driver);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return driverList;
    }

}


