package com.example.delivery.deliveryapp.model;

/**
 * Created by AdelaK on 2017-05-13.
 */

public class ProductModel {

    private String id;
    private String receiverAddress;
    private String receiverPostalCode;
    private String weight;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public String getReceiverPostalCode() {
        return receiverPostalCode;
    }

    public void setReceiverPostalCode(String receiverPostalCode) {
        this.receiverPostalCode = receiverPostalCode;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public ProductModel(){
        super();
    }
    public ProductModel(String id, String receiverAddress, String receiverPostalCode, String weight) {
        super();
        this.id = id;
        this.receiverAddress = receiverAddress;
        this.receiverPostalCode = receiverPostalCode;
        this.weight = weight;
    }

    @Override
    public String toString() {
       return String.format("Address: %s\nPostal Code: %s\nWeight: %s",
               this.receiverAddress, this.receiverPostalCode, this.weight);
    }
}
