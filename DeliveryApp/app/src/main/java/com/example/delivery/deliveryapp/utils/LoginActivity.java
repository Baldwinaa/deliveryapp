package com.example.delivery.deliveryapp.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.delivery.deliveryapp.R;
import com.example.delivery.deliveryapp.remote.ConnectivityChecker;
import com.example.delivery.deliveryapp.remote.Http;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private String response;
    private SharedPreferences userLoginPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        userLoginPrefs = getLoginPreferences();
        Button mLoginButton = (Button) findViewById(R.id.login_button);
        mLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    private void attemptLogin() {
        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            clear();
            cancel = true;
        }
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            clear();
            cancel = true;
        }
        if (new ConnectivityChecker().isNetworkAvailable(getApplicationContext())) {
            if (!isLoginValid(email, password)) {
                Toast.makeText(getApplicationContext(), getString(R.string.incorrect_login_password), Toast.LENGTH_LONG).show();
                clear();
                cancel = true;
            }
        }
        else {
            Toast.makeText(getApplicationContext(), getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
            clear();
            cancel = true;
        }
        if (!cancel) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    private boolean isLoginValid(String email, String password) {
        final Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put(getString(R.string.email), email);
        parameterMap.put(getString(R.string.password), password);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response = Http.Post(getString(R.string.login_url), parameterMap);
                }
                catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (response.equals("boss")) {
            setLoginPreferences(email, "admin");
            return true;
        }
        if (response.equals("driver")) {
            setLoginPreferences(email, "user");
            return true;
        }
        return false;
    }
    private SharedPreferences getLoginPreferences(){
        return getSharedPreferences("login_preference", Context.MODE_PRIVATE);
    }

    private void setLoginPreferences(String login, String role) {
        SharedPreferences.Editor editor = userLoginPrefs.edit();

        editor.putString(getString(R.string.login), login);
        editor.putString(getString(R.string.role), role);
        editor.apply();
    }

    private void clear(){
        SharedPreferences.Editor editor = userLoginPrefs.edit();
        editor.putString(getString(R.string.login), null);
        editor.putString(getString(R.string.role), null);
        editor.apply();
        mEmailView.setText(null);
        mPasswordView.setText(null);
    }

}

