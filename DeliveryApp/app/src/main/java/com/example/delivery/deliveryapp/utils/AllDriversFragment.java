package com.example.delivery.deliveryapp.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.example.delivery.deliveryapp.R;
import com.example.delivery.deliveryapp.model.DriverModel;
import com.example.delivery.deliveryapp.model.ProductModel;
import com.example.delivery.deliveryapp.remote.CustomExpandableListAdapter;
import com.example.delivery.deliveryapp.remote.Http;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by AdelaK on 2017-05-13.
 */

public class AllDriversFragment extends Fragment {
    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    HashMap<String, List<ProductModel>> expandableListDetail;
    String response;
    private List<DriverModel> driversList;
    private List<ProductModel>productsList;
    View view;


    public AllDriversFragment(){
//default constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_drivers_list,container,false);
        expandableListView = (ExpandableListView) view.findViewById(R.id.expandableListView);
        expandableListDetail = getData();
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new CustomExpandableListAdapter(this.getActivity(), expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        final int groupPosition, int childPosition, long id) {

                final int childPos =childPosition;
                new AlertDialog.Builder(getActivity())
                        .setTitle("Remove package")
                        .setMessage("Are you sure you want to remove this assignment?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                removeAssignment(driversList.get(groupPosition).getEmail().toString(),expandableListDetail.get(
                                        expandableListTitle.get(groupPosition)).get(
                                        childPos).getId().toString());
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
                return false;
            }
        });

        return view;
    }
    private HashMap<String, List<ProductModel>> getData() {
        String result = getList(getString(R.string.get_drivers_url));
        HashMap<String, List<ProductModel>> expandableListDetail = new HashMap<String, List<ProductModel>>();
        try {
            driversList = getListOfDrivers(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (DriverModel i : driversList)
        {
            List<String> products = new ArrayList<String>();
            result = getListOfProducts(i.getEmail());
            try {
                productsList = getListOfProductsModel(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            i.setProducts(productsList);
            expandableListDetail.put(i.toString(),i.getProducts());
        }
        return expandableListDetail;
    }

    private String getList(final String url) {
        response = "";
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response = Http.Get(url,null);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    private List<DriverModel> getListOfDrivers(String result) throws JSONException {
        JSONArray jsonArray = new JSONArray(result);
        List driverList = new ArrayList<DriverModel>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            DriverModel driver = new DriverModel();
            try {
                driver.setEmail(jsonObject.get("email").toString());
                driver.setName(jsonObject.get("name").toString());
                driver.setLastName(jsonObject.get("lastName").toString());
                driverList.add(driver);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return driverList;
    }

    public String getListOfProducts(String email) {
        response = "";
        final Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put(getString(R.string.dEmail), email);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response = Http.Get(getString(R.string.get_packages_for_driver_url),parameterMap);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return response;
    }

    private List getListOfProductsModel(String result) throws JSONException {
        JSONObject json = new JSONObject(result);
        String listOfProducts = json.getString("packages").toString();
        JSONArray jsonArray = new JSONArray(listOfProducts);
        List productList = new ArrayList<ProductModel>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            ProductModel product = new ProductModel();
            try {
                product.setId(jsonObject.get("_id").toString());
                product.setReceiverAddress(jsonObject.get("receiverAddress").toString());
                product.setReceiverPostalCode(jsonObject.get("receiverPostalCode").toString());
                product.setWeight(jsonObject.get("weight").toString());
                productList.add(product);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return productList;
    }
    private void removeAssignment(String email, String product) {
        final Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put(getString(R.string.dEmail), (email));
        parameterMap.put(String.format("%s[0]", getString(R.string.packages_Id)), product);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    response = Http.Post(getString(R.string.remove_package), parameterMap);

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        getFragmentManager().beginTransaction().replace(R.id.content_main, new AllDriversFragment()).commit();
    }

}



