package com.example.delivery.deliveryapp.remote;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Properties;

/**
 * Created by AdelaK on 2017-04-24.
 */

public class Http {

    public static String Post(String uri,Map<String,String> parameterList) {
        HttpURLConnection urlConnection;
        Properties systemProperties = System.getProperties();
        systemProperties.setProperty("http.proxyPort", "80");
        String result = null;
        try {
            urlConnection = (HttpURLConnection) ((new URL(uri).openConnection()));
            urlConnection.setDoOutput(true);
            urlConnection.setInstanceFollowRedirects(false);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("charset", "utf-8");
            urlConnection.setConnectTimeout(5000);
            urlConnection.setReadTimeout(5000);
            urlConnection.connect();
            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, String> param : parameterList.entrySet())
            {
                if (postData.length() != 0) postData.append('&');
                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
            }
            OutputStream outputStream = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            writer.write(postData.toString());
            writer.close();
            outputStream.close();

            BufferedReader bufferedReader=null;
            if(urlConnection.getResponseCode()==200) {
                bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
            }
            else {
                bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream(), "UTF-8"));
            }
            String line;
            StringBuilder sb = new StringBuilder();

            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            bufferedReader.close();
            result = sb.toString();
            urlConnection.disconnect();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String Get(String uri, Map<String,String> parameterList) {

        HttpURLConnection urlConnection = null;
        StringBuilder newUri = new StringBuilder(uri);
        if(parameterList!=null){
            for (Map.Entry<String, String> param : parameterList.entrySet()) {
                newUri.append("/?");
                newUri.append(param.getKey());
                newUri.append('=');
                newUri.append(param.getValue());
            }
        }
        try {
            urlConnection = (HttpURLConnection) ((new URL(newUri.toString()).openConnection()));
            urlConnection.setRequestMethod("GET");
            BufferedReader bufferedReader=null;
            int code = urlConnection.getResponseCode();
            if(urlConnection.getResponseCode()==200) {
                bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
            }
            else{
                bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream(), "UTF-8"));
            }
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            bufferedReader.close();
            return sb.toString();

        }
        catch (Exception e) {
            e.printStackTrace();}
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }
}
